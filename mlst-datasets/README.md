# README #

### MLST data sets considered ###

In this study, we've used data sets retrieved on 2014-06-24 from MLST.net, PubMLST.org and Institute Pasteur, from nine different bacterial species:
*B. pseudomallei*, *C. jejuni*, *E. faecium*, *H. influenzae*, *Neisseria spp.*, *P. aeruginosa*, *S. agalactiae*, *S. aureus* and *S. pneumoniae*.

