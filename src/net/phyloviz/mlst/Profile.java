package net.phyloviz.mlst;

public class Profile implements Comparable<Profile> {
	public static int PROFILE_SIZE = 0;
	protected int[] iaAllelesIDs;

	public Profile() {
		iaAllelesIDs = new int[PROFILE_SIZE];
	}

	public Profile(int[] alleles) {
		iaAllelesIDs = alleles;
	}
	
	public final int getAlleleIDatPos(int i) {
		return iaAllelesIDs[i];
	}

	public final void setAlleleAtPos(int i, int allele) {
		iaAllelesIDs[i] = allele;
	}

	public final int getNdiff(Profile st) {
		int diffs, i;

		for (diffs = i = 0; i < iaAllelesIDs.length; i++)
			if (iaAllelesIDs[i] != st.iaAllelesIDs[i])
				diffs++;
		return diffs;
	}

	public final int compareTo(Profile st) {
		int ret, i;

		for (ret = i = 0; ret == 0 && i < iaAllelesIDs.length; i++)
			ret = iaAllelesIDs[i] - st.iaAllelesIDs[i];

		return ret;
	}

	protected static void setProfileSize(int sz) {
		if (PROFILE_SIZE == 0 && sz > 0)
			PROFILE_SIZE = sz;
	}

	public final String toString() {
		String retObj = "";
		for (int i = 0; i < Profile.PROFILE_SIZE; i++)
			retObj += " " + getAlleleIDatPos(i);
		return retObj;
	}
}
