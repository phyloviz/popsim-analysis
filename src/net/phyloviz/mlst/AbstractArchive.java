package net.phyloviz.mlst;

import java.util.Collection;
import java.util.Set;
import java.util.TreeMap;

import net.phyloviz.util.DisjointSet;

public abstract class AbstractArchive {
	protected TreeMap<Integer, Profile> stID2Profile;
	protected TreeMap<Profile, Integer> profile2stID;

	public AbstractArchive() {
		stID2Profile = new TreeMap<Integer, Profile>();
		profile2stID = new TreeMap<Profile, Integer>();
	}
	
	public final int getSTid(Profile p) {
		Integer id = profile2stID.get(p);
		return (id == null) ? 0 : id.intValue();
	}

	public abstract Profile getProfile(int id);
	
	public abstract int getSTid(int id);
	
	public abstract int getSTindex(int id);
	
	public abstract int size();

	public abstract int getLV(int st, int level);
	
	public abstract void updateLVs(DisjointSet set);

	public abstract int getFreq(int st);
	
	public final Set<Integer> getAllSTids() {
		return stID2Profile.keySet();
	}

	public String getAllelicProfile(Collection<Integer> collection) {
		StringBuffer sb = new StringBuffer();
		for (int st : collection) {
			sb.append(st);
			Profile pf = (Profile)stID2Profile.get(st);
			for (int a = 0; a < Profile.PROFILE_SIZE; a++) {
				sb.append("\t").append(pf.getAlleleIDatPos(a));
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
