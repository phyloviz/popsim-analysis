package net.phyloviz.mlst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import net.phyloviz.mlst.AbstractArchive;
import net.phyloviz.util.DisjointSet;

public class Archive extends AbstractArchive {

	public final static int MAXLV = 3;

	private int[] indivSTids;
	private int[] freq;
	private TreeSet<Integer> tsSTs;
	private HashMap<Integer,Integer> STid2index;
	private int[][] lv;
	private int idx;

	public Archive(int popsize) {
		super();

		idx = 0;
		indivSTids = new int[popsize];
		tsSTs = new TreeSet<Integer>();
		STid2index = new HashMap<Integer, Integer>();
		lv = new int[popsize][MAXLV];
		freq = new int[popsize];
	}

	public boolean add(int iSTid, Profile profile) {
		if (idx >= indivSTids.length)
			return false;

		if (profile2stID.get(profile) == null) {
			stID2Profile.put(iSTid, profile);
			profile2stID.put(profile, iSTid);

			indivSTids[idx] = iSTid;
			STid2index.put(iSTid, idx);
			tsSTs.add(iSTid);

			idx++;
		}

		iSTid = profile2stID.get(profile);
		freq[getSTindex(iSTid)] ++;

		return true;
	}

	public Profile getProfile(int id) {
		return (Profile) stID2Profile.get(id);
	}

	public int getSTid(int i) {
		return indivSTids[i];
	}

	public int getSTindex(int STid) {
		return STid2index.get(STid);
	}
	
	public void setSTid(int i, int stid) {
		indivSTids[i] = stid;
	}

	public TreeSet<Integer> getUniqueSTs() {
		return tsSTs;
	}

	public int size() {
		if (indivSTids != null)
			return indivSTids.length;
		return 0;
	}

	private void addLV(int st, int diff) {
		if (diff <= MAXLV)
			lv[getSTindex(st)][diff - 1] ++;
	}

	public void updateLVs(DisjointSet set) {
		List<Integer> alUniqueSTs = new ArrayList<Integer>(getAllSTids());
		for (int i = 0; i < (alUniqueSTs.size() - 1); i++) {
			for (int j = i + 1; j < alUniqueSTs.size(); j++) {
				int st1 = alUniqueSTs.get(i);
				int st2 = alUniqueSTs.get(j);
				int diff = getProfile(st1).getNdiff(getProfile(st2));

				if (set != null && set.sameSet(getSTindex(st1), getSTindex(st2))) {
					addLV(st1, diff);
					addLV(st2, diff);
				}
			}
		}	
	}
	
	public int getLV(int st, int level) {
		return lv[getSTindex(st)][level - 1];
	}
	
	public int getFreq(int st) {
		return freq[getSTindex(st)];
	}
}
