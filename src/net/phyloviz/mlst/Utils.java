package net.phyloviz.mlst;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;

public class Utils {

	public static boolean isHeader(String sLine) {
		boolean bRetObj = false;
		if (sLine != null) {
			String[] saTmp = sLine.split("\t");
			if (saTmp != null && saTmp.length > 1) {
				try {
					Integer.parseInt(saTmp[0]);
				} catch (NumberFormatException e) {
					bRetObj = true;
				}
			}
		}
		return bRetObj;
	}

	public static float SimpsonsIndexDiversity(ArrayList<Integer> alSTs) {
		float fSID = 0;
		int size = alSTs.size();
		if (alSTs != null && size > 0) {
			TreeMap<Integer, Integer> idCount = new TreeMap<Integer, Integer>();
			for (int i = 0; i < size; i++) {
				int st = alSTs.get(i);
				if (idCount.containsKey(st))
					idCount.put(st, idCount.get(st) + 1);
				else
					idCount.put(st, 1);
			}
			for (int profile : idCount.keySet()) {
				int count = idCount.get(profile);
				fSID += count * (count - 1);
			}
			// This separation avoids an integer overflow
			fSID /= size;
			fSID /= (size - 1);
		}
		return 1 - fSID;
	}

	public static boolean writeFile(String fileName, String content) {
		try {
			FileWriter fstream = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fstream);
			bw.write(content);
			bw.close();
			fstream.close();
		} catch (IOException e) {
			System.err.println("IOException: " + fileName);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static ArrayList<String> readStream(BufferedReader br)
			throws IOException {
		ArrayList<String> al = new ArrayList<String>();
		String strLine;
		while ((strLine = br.readLine()) != null)
			al.add(strLine);
		br.close();
		return al;
	}

	// TODO: Change this to load FASTA file
	public static ArrayList<TreeMap<Integer, String>> loadAllelicSequences(
			String sProfileFile) throws NumberFormatException, IOException {
		ArrayList<TreeMap<Integer, String>> alSeqs = new ArrayList<TreeMap<Integer, String>>();
		if (!sProfileFile.endsWith(".st.csv")) {
			System.out.println("Expecting suffix <name>.st.csv on file "
					+ sProfileFile + "!!!");
			return alSeqs;
		}
		for (int i = 1; i <= Profile.PROFILE_SIZE; i++) {
			//TreeMap<Integer, String> tmTmp = new TreeMap<Integer, String>();
			int length = sProfileFile.length();
			String sSeqFile = sProfileFile.substring(0, length - 7) + ".g" + i
					+ ".tfa";// + sProfileFile.substring(length - 3);

//			for (String s : Utils.readFile(sSeqFile, false)) {
//				String[] sa = s.split("\t");
//				tmTmp.put(Integer.parseInt(sa[0]), sa[1]);
//			}
			alSeqs.add(readFastaFile(sSeqFile));
		}
		return alSeqs;
	}

	private static TreeMap<Integer, String> readFastaFile(String fileName)
			throws IOException {
		TreeMap<Integer, String> al = new TreeMap<Integer, String>();
		DataInputStream in = new DataInputStream(new FileInputStream(fileName));
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String curr = "", strLine;

		while ((strLine = br.readLine()) != null) {
			if (strLine.charAt(0) == '>') {
				if (!curr.isEmpty())
					al.put(al.size(), curr);
				curr = "";
			} else {
				curr += strLine;
			}
		}
		br.close();
		if (!curr.isEmpty())
			al.put(al.size(), curr);
		return al;
	}

	public static ArrayList<String> readFile(String fileName, boolean bHeader)
			throws IOException {
		ArrayList<String> al = new ArrayList<String>();
		DataInputStream in = new DataInputStream(new FileInputStream(fileName));
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		if (!bHeader)
			br.readLine();
		while ((strLine = br.readLine()) != null)
			al.add(strLine);
		in.close();
		return al;
	}

	public static void saveUrl(String filename, String urlString)
			throws MalformedURLException, IOException {
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
			in = new BufferedInputStream(new URL(urlString).openStream());
			fout = new FileOutputStream(filename);

			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
			}
		} finally {
			if (in != null)
				in.close();
			if (fout != null)
				fout.close();
		}
	}
}
