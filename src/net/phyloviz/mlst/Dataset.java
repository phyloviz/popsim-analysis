package net.phyloviz.mlst;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Dataset {

	public static AbstractArchive loadProfileFile(String file)
			throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		return Dataset.loadProfile(br);
	}

	public static AbstractArchive loadProfile(BufferedReader br)
			throws IOException {
		ArrayList<String> alTmp = Utils.readStream(br);
		String[] saTmp = alTmp.get(0).split("\\s");
		// profile: ST ... (clonal_complex?)
		if (saTmp != null
				&& (saTmp[saTmp.length - 1].equals("clonal_complex") || saTmp[saTmp.length - 1]
						.equals("mlst_clade")))
			Profile.setProfileSize(saTmp.length - 2);
		else
			Profile.setProfileSize(saTmp.length - 1);

		// Ex: ST dnaE gyrB recA dtdS pntA pyrC tnaA (clonal_complex?)
		if (Utils.isHeader(alTmp.get(0))) {
			alTmp.remove(0);
		}

		Archive archive = new Archive(alTmp.size());
		for (int i = 0; i < alTmp.size(); i++) {
			saTmp = alTmp.get(i).split("\\s");
			int[] alleles = new int[Profile.PROFILE_SIZE];
			for (int a = 0; a < Profile.PROFILE_SIZE; a++)
				alleles[a] = Integer.parseInt(saTmp[a + 1]);
			int stid = Integer.parseInt(saTmp[0]);
			archive.add(stid, new Profile(alleles));
		}
		return archive;
	}
}
