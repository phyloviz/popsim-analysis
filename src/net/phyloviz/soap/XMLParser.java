package net.phyloviz.soap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class XMLParser {

	private static final String END_POINT = "http://www.phyloviz.net/data/databases.xml";
	private static final String DEFAULT_URL = "http://www.google.com";

	private static XMLParser parser = null;
	private static Document document = null;
	private static ArrayList<String> alDatabases = null;

	private XMLParser() {
		loadXML();
	}

	private void loadXML() {
		try {
			SAXBuilder builder = new SAXBuilder();
			document = builder.build(new URL(END_POINT));
		} catch (IOException e) {
			// No internet connection
			document = null;
		} catch (JDOMException e) {
			// JDOM Error Parsing XML
			document = null;
		}
	}

	public static boolean hasConnection() {
		boolean bHas = false;
		try {
			URL newURL = new URL(DEFAULT_URL);
			InputStream is = newURL.openStream();
			is.close();
			bHas = true;
		} catch (IOException e) {
			Logger.getLogger(XMLParser.class.getName()).log(Level.WARNING,
					e.getLocalizedMessage());
		}
		return bHas;
	}

	public static XMLParser getParser() {
		if (parser == null) {
			parser = new XMLParser();
		}
		return parser;
	}

	public ArrayList<String> getDatabaseList() {
		if (document == null) {
			return null;
		}
		if (alDatabases != null) {
			return alDatabases;
		}

		alDatabases = new ArrayList<String>();
		for (Object o : document.getRootElement().getChildren("species")) {
			Element species = (Element) o;
			alDatabases.add(species.getChild("name").getText());
		}
		return alDatabases;
	}

	public String getDatabaseAcronym(int index) {
		if (document == null) {
			return null;
		}
		String acronym;
		Element e = (Element) document.getRootElement().getChildren("species")
				.get(index);
		Element id = e.getChild("mlst").getChild("database").getChild("id");
		if (id != null) {
			// Pasteur / EcMLST
			acronym = id.getText();
		} else {
			// mlst.net / pubmlst.org
			String s = e.getChild("mlst").getChild("database").getChild("profiles")
					.getChild("url").getText();
			acronym = s.substring(s.lastIndexOf("/")+1, s.lastIndexOf("."));
		}
		return acronym;
	}

	public String getProfileURL(int index) {
		if (document == null) {
			return null;
		}
		Element e = (Element) document.getRootElement().getChildren("species")
				.get(index);
		return e.getChild("mlst").getChild("database").getChild("profiles")
				.getChild("url").getText();
	}

	public ArrayList<String> getLociURLs(int index) {
		if (document == null) {
			return null;
		}
		ArrayList<String> alTmp = new ArrayList<String>();
		Element e = (Element) document.getRootElement().getChildren("species")
				.get(index);
		Element loci = e.getChild("mlst").getChild("database").getChild("loci");
		for (Object locus : loci.getChildren("locus")) {
			alTmp.add(((Element) locus).getChild("url").getText());
		}
		return alTmp;
	}

	private String readSequence(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line, s = "";
		while ((line = br.readLine()) != null) {
			s += line + "\n";
		}
		return s.trim();
	}

	public String getLocusSequence(String sDB, int iLocus) throws IOException {
		if (document == null) {
			return null;
		}
		int iDB = alDatabases.indexOf(sDB);
		Element e = (Element) document.getRootElement().getChildren("species")
				.get(iDB);
		Element loci = e.getChild("mlst").getChild("database").getChild("loci");
		Element locus = (Element) loci.getChildren("locus").get(iLocus);

		URL url = new URL(locus.getChild("url").getText());
		return readSequence(url.openStream());
	}

	public ArrayList<String> getLoci(int index) {
		ArrayList<String> alRet = new ArrayList<String>();
		if (document != null) {
			Element e = (Element) document.getRootElement()
					.getChildren("species").get(index);
			Element loci = e.getChild("mlst").getChild("database")
					.getChild("loci");
			for (Object o : loci.getChildren("locus")) {
				alRet.add(((Element) o).getChild("name").getText());
			}
		}
		return alRet;
	}

	public void reset() {
		parser = null;
		document = null;
		alDatabases = null;
	}
}
