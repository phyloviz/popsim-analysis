package net.phyloviz.soap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.phyloviz.mlst.Utils;

public class Download {

	public static void main(String[] args) throws IOException {
		if (args == null || args.length < 1)
			return;

		List<String> lDatabases = XMLParser.getParser().getDatabaseList();
		if (args.length == 1 && args[0].equals("0")) {
			for (int i = 0; i < lDatabases.size(); i++) {
				System.out.println((i + 1) + " - " + XMLParser.getParser().getDatabaseAcronym(i) + " - "+ lDatabases.get(i));
			}
			return;
		} else if (args.length == 2) {
			// 1st arg: DB index
			// 2nd arg: output dir

			int iDB = Integer.parseInt(args[0]) - 1;
			String sDB = lDatabases.get(iDB);
			String acronym = XMLParser.getParser().getDatabaseAcronym(iDB);
			System.out.println(sDB + " - " + acronym);

			// Create Directory
			File fOutDir = new File(args[1] + "/" + acronym);
			fOutDir.mkdirs();

			// Download ST Profile
			String stURL = XMLParser.getParser().getProfileURL(iDB);
			Utils.saveUrl(
					fOutDir.getAbsolutePath() + "/" + acronym + ".st.csv",
					stURL);

			// Download Allelic Sequences
			ArrayList<String> alLoci = XMLParser.getParser().getLociURLs(iDB);
			for (int i = 0; i < alLoci.size(); i++) {
				Utils.saveUrl(fOutDir.getAbsolutePath() + "/" + acronym + ".g"
						+ (i + 1) + ".tfa", alLoci.get(i));
			}
		}
	}
}
