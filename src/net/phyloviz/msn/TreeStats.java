package net.phyloviz.msn;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.phyloviz.mlst.AbstractArchive;
import net.phyloviz.mlst.Archive;
import net.phyloviz.mlst.Dataset;
import net.phyloviz.nlvgraph.GraphNLV;
import net.phyloviz.util.DisjointSet;
import no.uib.cipr.matrix.DenseLU;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.UpperTriangDenseMatrix;

public class TreeStats {

	public static int poolSize = 4;
	
	public static void main(String[] argv) {
		AbstractArchive archive = null;
		int level = 1;
		int ruleLevel = 0;
		
		if (argv != null && argv.length >= 1)
			level = Integer.parseInt(argv[0]);
		
		if (argv != null && argv.length >= 2)
			poolSize = Integer.parseInt(argv[1]);
		
		if (argv != null && argv.length >= 3)
			ruleLevel = Integer.parseInt(argv[2]);
		
		if (argv != null && argv.length >= 4) {
			try {
				archive = Dataset.loadProfileFile(argv[3]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				archive = Dataset.loadProfile(br);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		GraphNLV graph = new GraphNLV(archive);
		// Build until level n
		graph.build(level);
		
		// Find clusters or CCs
		List<Integer> alUniqueSTs = new ArrayList<Integer>(archive.getAllSTids());
		DisjointSet cds = new DisjointSet(alUniqueSTs.size());
		
		for (int st : alUniqueSTs)
			for (int i = 1; i <= level; i++) 
				if (graph.getNLVAdjacency(i, st) != null)
					for (int x : graph.getNLVAdjacency(i, st))
						cds.unionSet(archive.getSTindex(st), archive.getSTindex(x));
	
		TreeMap<Integer,TreeSet<Integer>> ccs = new TreeMap<Integer, TreeSet<Integer>>();
		for (int i = 0; i < alUniqueSTs.size(); i++) {
			if (ccs.get(cds.findSet(i)) == null)
				ccs.put(cds.findSet(i), new TreeSet<Integer>());
			ccs.get(cds.findSet(i)).add(i);
		}
		
		LinkedList<TreeSet<Integer>> groups = new LinkedList<TreeSet<Integer>>(ccs.values()); 
		Collections.sort(groups, new Comparator<TreeSet<Integer>>() {

			@Override
			public int compare(TreeSet<Integer> o1, TreeSet<Integer> o2) {
				return o2.size() - o1.size();
			}
		});
		//for (TreeSet<Integer> test : groups) {
		//	System.out.print(test.size() + ":");
		//	for (int k : test)
		//		System.out.print(" " + archive.getSTid(k));
		//	System.out.println();
		//}

		// Update stats
		archive.updateLVs(cds);
		
		//for (int st : archive.getAllSTids()) {
		//	int i = archive.getSTindex(st);
		//	System.out.println(i + " "
		//			+ archive.getLV(st, 1) + " "
		//			+ archive.getLV(st, 2) + " "
		//			+ archive.getLV(st, 3) + " "
		//			+ archive.getFreq(st) + " "
		//			+ st);
		//}
		
		List<EdgeMST> edgesList = new ArrayList<EdgeMST>();
		Iterator<TreeSet<Integer>> gIter = groups.iterator();

		int gid = 0;
		while (gIter.hasNext()) {
			
			TreeSet<Integer> g = gIter.next();
			for (int st : g)
				for (int i = 1; i <= level; i++) 
					if (graph.getNLVAdjacency(i, archive.getSTid(st)) != null)
						for (int x : graph.getNLVAdjacency(i, archive.getSTid(st)))
							if (st < archive.getSTindex(x))
								edgesList.add(new EdgeMST(st, archive.getSTindex(x), i));
			
			if (edgesList.isEmpty()) {
				continue;
			}
			
			// Let us filter edges
			if (ruleLevel > 0) {
				EdgeComparator cmp = new EdgeComparator(archive, ruleLevel);
				List<EdgeMST> edgesTmpList = new ArrayList<EdgeMST>(); 
				Collections.sort(edgesList, cmp);
				cds = new DisjointSet(alUniqueSTs.size());
				
				EdgeMST f = null;
				for (EdgeMST e : edgesList) {
					if (!cds.sameSet(e.getSource(), e.getDest()) || (f != null && cmp.compare(f, e) == 0)) {
						cds.unionSet(e.getSource(), e.getDest());
						edgesTmpList.add(e);
						f = e;
					}
				}
				
				edgesList = edgesTmpList;
			}

			System.out.println("Processing CC " + gid + "...");
			double nmsts = calcNumberMSTs(edgesList);

			System.out.println("CC " + gid + " has 10^" + nmsts + " = " + Math.pow(10, nmsts) + " MSTs");
			
			System.out.println("Edge stats:");
			//System.out.println("Skipping edge stats.");
			
			Iterator<EdgeMST> ei = edgesList.iterator();
			while (ei.hasNext()) {
				EdgeMST e = ei.next();
				System.out.print(archive.getSTid(e.getSource()) + " - " + archive.getSTid(e.getDest())
						+ ", level: " + e.getLevel() + ", ");
				System.out.print("freq: " + (Math.pow(10, e.getNmsts()) * 100.0) + "% (10^" + e.getNmsts() + ")");
				System.out.println(" 10^" + e.getAbsoluteNmsts() + "/10^" + e.getDeltaNmsts());
			}

			System.out.println("CC " + gid + " done.\n");

			gid ++;

			edgesList.clear();
		}
	}
	
	private static class EdgeComparator implements Comparator<EdgeMST> {

		private AbstractArchive archive;
		private int ruleLevel;
		
		EdgeComparator(AbstractArchive archive, int ruleLevel) {
			this.archive = archive;
			this.ruleLevel = ruleLevel;
		}
		
		@Override
		public int compare(EdgeMST f, EdgeMST e) {
			
			int ret = 0; 
			int k = 0; 
			int lv = 0; 
			ret = f.getLevel() - e.getLevel(); 
			
			if (ret != 0) {
				return ret; 
			} 
	
			int fU = archive.getSTid(f.getSource()); 
			int fV = archive.getSTid(f.getDest()); 
			int eU = archive.getSTid(e.getSource()); 
			int eV = archive.getSTid(e.getDest()); 
				
			while (k < Archive.MAXLV && k < ruleLevel) {
				ret = Math.max(archive.getLV(fU, k+1), archive.getLV(fV, k+1))
						- Math.max(archive.getLV(eU, k+1), archive.getLV(eV, k+1)); 
				lv ++; 
				
				if (ret != 0) { 
					break; 
				} 
				
				ret = Math.min(archive.getLV(fU, k+1), archive.getLV(fV, k+1))
						- Math.min(archive.getLV(eU, k+1), archive.getLV(eV, k+1));
				lv ++;
				
				if (ret != 0) {
					break;
				}
				
				k++;	
			}
			
			/* SAT is ignored! */
			
			/* ST frequency. */
			if (k >= Archive.MAXLV && k < ruleLevel) {
				lv = 2*Archive.MAXLV + 2;
				ret = Math.max(archive.getFreq(fU), archive.getFreq(fV))
						- Math.max(archive.getFreq(eU), archive.getFreq(eV));
				lv ++; 
				
				if (ret == 0) {
					ret = Math.min(archive.getFreq(fU), archive.getFreq(fV))
							- Math.min(archive.getFreq(eU), archive.getFreq(eV));
					lv ++;
				}
				
				k++ ;
			} 
			
			/* Decreasing... */
			ret *= -1;
			
			/* Last chance... */
			if (ret == 0 && k < ruleLevel) {
			
				ret = Math.min(fU, fV) - Math.min(eU, eV);
				if (ret == 0)
					ret = Math.max(fU, fV) - Math.max(eU, eV);	
				
				lv += 2;
			} 
			
			return Integer.signum(ret)*lv;
		}	
	}

	private static class EdgeTask implements Runnable {
		
		private int[] map;
		private int[] mapaux;
		private ArrayList[] calcDet;
		private DenseMatrix matrix; 
		private double[] calcNMSTs;	
		private EdgeMST edge;
		
		EdgeTask(EdgeMST edge, int[] map,  int[] mapaux,  ArrayList[] calcDet,  DenseMatrix matrix,  double[] calcNMSTs) {
			this.map = map;
			this.mapaux = mapaux;
			this.calcDet = calcDet;
			this.matrix = matrix;
			this.calcNMSTs = calcNMSTs;
			this.edge = edge;
		}
		
		@Override
		public void run() {
			edge.setNmsts(map, mapaux, calcDet, matrix, calcNMSTs);
		}	
	}
	
	private static void calcEdgesNMSTs(List<EdgeMST> edgesList, int prev, int now,  int[] map,  int[] mapaux,  ArrayList[] calcDet,  DenseMatrix matrix,  double[] calcNMSTs) {
		
		ExecutorService es = Executors.newFixedThreadPool(poolSize);
		
		for (int i = prev; i < now; i++) {
			EdgeTask et = new EdgeTask(edgesList.get(i), map, mapaux, calcDet, matrix, calcNMSTs);
			es.submit(et);
		}
	
		es.shutdown();
		try {
			es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			es.shutdownNow();
		}
	}

	private static int findMaxId(List<EdgeMST> edgesList) {
		int id = 0;
		Iterator<EdgeMST> eIter = edgesList.iterator();
		while (eIter.hasNext()) {
			EdgeMST e = (EdgeMST) eIter.next();
			id = Math.max(id, e.getSource());
			id = Math.max(id, e.getDest());
		}


		return id;
	}

	private static double calcNumberMSTs(List<EdgeMST> edgesList) {

		Collections.sort(edgesList);

		double nmsts = 0;

		int mapid;

		//Passo 1 - Descobrir o MaxId Inicial
		int maxid = findMaxId(edgesList);
		DenseMatrix matrix = new DenseMatrix(maxid + 1, maxid + 1);
		matrix.zero();

		DisjointSet ds = new DisjointSet(maxid);

		int[] map = new int[maxid + 1];
		for (int i = 0; i <= maxid; i++) {
			map[i] = i;
		}

		int[] mapaux = new int[maxid + 1];
		for (int i = 0; i <= maxid; i++) {
			mapaux[i] = -1;
		}


		//Passo 2 - Varrer arcos do nivel L, preenchendo a matriz
		Iterator<EdgeMST> eIter = edgesList.iterator();

		EdgeMST e = eIter.next();
		int level = (e != null) ? e.getLevel() : 1;

		ArrayList<Integer> vaux = new ArrayList<Integer>(maxid + 1);

		int prev = 0;
		int now = 0;

		while (true) {
			if (e != null && e.getLevel() == level) {
				int u = e.getSource();
				int v = e.getDest();
				if (!vaux.contains(u)) {
					vaux.add(u);
				}
				if (!vaux.contains(v)) {
					vaux.add(v);
				}
				//Preenchimento da Matriz
				int s = map[u];
				int d = map[v];

				matrix.set(s, d, matrix.get(s, d) - 1);
				matrix.set(d, s, matrix.get(d, s) - 1);
				matrix.set(s, s, matrix.get(s, s) + 1);
				matrix.set(d, d, matrix.get(d, d) + 1);

				if (!ds.sameSet(u, v)) {
					ds.unionSet(u, v);
				}

				now++;

				try {
					e = eIter.next();
				} catch (NoSuchElementException ex) {
					e = null;
				}

			} else if (prev != now ) {
				mapid = 0;
				for (int i = 0; i <= maxid; i++) {
					int setid = ds.findSet(i);
					if (mapaux[setid] == -1) {
						mapaux[setid] = mapid;
						mapaux[i] = mapid;
						mapid++;
					} else {
						mapaux[i] = mapaux[setid];
					}
				}

				ArrayList[] calcDet = new ArrayList[mapid];
				for (int i = 0; i < calcDet.length; i++) {
					calcDet[i] = new ArrayList<Integer>();
				}

				for (int i = 0; i < vaux.size(); i++) {
					if (!calcDet[mapaux[vaux.get(i)]].contains(map[vaux.get(i)])) {
						calcDet[mapaux[vaux.get(i)]].add(map[vaux.get(i)]);
					}
				}

				//System.out.println("Level " + level);

				double[] calcNMstsDet = new double[mapid];
				for (int i = 0; i < calcDet.length; i++) {

					if (!calcDet[i].isEmpty()) {
						
						// Check Kirchhoff's theorem!
						int[] vgraph = new int[calcDet[i].size() - 1];
						ArrayList<Integer> graph = (ArrayList<Integer>) calcDet[i];
						Iterator<Integer> gIter = graph.iterator();
						int index = 0;
						gIter.next();
						while (gIter.hasNext()) {
							vgraph[index++] = gIter.next();
						}
						
						UpperTriangDenseMatrix u = DenseLU.factorize(Matrices.getSubMatrix(matrix, vgraph, vgraph).copy()).getU();
						double det = 0.0;
						for (int k = 0; k < u.numRows(); k++)
							det += Math.log10(Math.abs(u.get(k, k)));
						
						calcNMstsDet[i] = det;
						nmsts = nmsts + det;
					}
				}

				calcEdgesNMSTs(edgesList, prev, now, map, mapaux, calcDet, matrix, calcNMstsDet);
				prev = now;
				if (e == null) {
					break;
				}

				matrix = new DenseMatrix(mapid, mapid);
				matrix.zero();
				map = mapaux;
				mapaux = new int[maxid + 1];
				for (int i = 0; i <= maxid; i++) {
					mapaux[i] = -1;
				}
				//Passa para o nível seguinte
				level++;
				vaux = new ArrayList<Integer>(mapid);
			} else { // Se prev==now a lista é vazia e passamos para o nível seguinte
				level++;
			}
		}

		return nmsts;
	}
}
