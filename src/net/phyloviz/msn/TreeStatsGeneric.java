package net.phyloviz.msn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.phyloviz.util.DisjointSet;
import no.uib.cipr.matrix.DenseLU;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.UpperTriangDenseMatrix;

public class TreeStatsGeneric {

	public static int poolSize = 4;
	
	public static void main(String[] argv) {

		List<EdgeMST> graph = new ArrayList<EdgeMST>();
		int maxid = 0;

		try {
			graph = loadDataset(argv[0]);
			maxid = Integer.valueOf(argv[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		double nmsts = calcNumberMSTs(graph, maxid);
		System.out.println("The Graph has 10^" + nmsts + " = " + Math.pow(10,nmsts) + " MSTs");
		System.out.println("Edge stats:");
		Iterator<EdgeMST> ei = graph.iterator();
		while (ei.hasNext()) {
			EdgeMST e = ei.next();
			System.out.print(e.getSource() + " " + e.getDest() + " ");
			System.out.println((Math.pow(10, e.getNmsts())));
		}

		System.out.println("Graph done.\n");
	}
	

	private static List<EdgeMST> loadDataset(String filename) throws IOException {
		List<EdgeMST> graph = new ArrayList<EdgeMST>();
		BufferedReader input = new BufferedReader(new FileReader(filename));
		String line = null;
		line = input.readLine();
		while(line != null){
			String[] s = line.split("\t");
			try {
				int src = new Integer(s[0]);
				int target = new Integer(s[1]);
        int w = 1;

        if (s.length > 2)
				  w = Integer.parseInt(s[2]);

				EdgeMST edge = new EdgeMST(src, target, w);

				graph.add(edge);
			} catch (ArrayIndexOutOfBoundsException e) {
				throw e;
			}
			line = input.readLine();
		}
		
		return graph;
	}


	private static class EdgeTask implements Runnable {
		
		private int[] map;
		private int[] mapaux;
		private ArrayList[] calcDet;
		private DenseMatrix matrix; 
		private double[] calcNMSTs;	
		private EdgeMST edge;
		
		EdgeTask(EdgeMST edge, int[] map,  int[] mapaux,  ArrayList[] calcDet,  DenseMatrix matrix,  double[] calcNMSTs) {
			this.map = map;
			this.mapaux = mapaux;
			this.calcDet = calcDet;
			this.matrix = matrix;
			this.calcNMSTs = calcNMSTs;
			this.edge = edge;
		}
		
		@Override
		public void run() {
			edge.setNmsts(map, mapaux, calcDet, matrix, calcNMSTs);
		}	
	}
	
	private static void calcEdgesNMSTs(List<EdgeMST> edgesList, int prev, int now,  int[] map,  int[] mapaux,  ArrayList[] calcDet,  DenseMatrix matrix,  double[] calcNMSTs) {
		
		ExecutorService es = Executors.newFixedThreadPool(poolSize);
		
		for (int i = prev; i < now; i++) {
			EdgeTask et = new EdgeTask(edgesList.get(i), map, mapaux, calcDet, matrix, calcNMSTs);
			es.submit(et);
		}
	
		es.shutdown();
		try {
			es.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			es.shutdownNow();
		}
	}

	private static double calcNumberMSTs(List<EdgeMST> edgesList, int maxid) {

		Collections.sort(edgesList);

		double nmsts = 0;

		int mapid;

		DenseMatrix matrix = new DenseMatrix(maxid + 1, maxid + 1);
		matrix.zero();

		DisjointSet ds = new DisjointSet(maxid);

		int[] map = new int[maxid + 1];
		for (int i = 0; i <= maxid; i++) {
			map[i] = i;
		}

		int[] mapaux = new int[maxid + 1];
		for (int i = 0; i <= maxid; i++) {
			mapaux[i] = -1;
		}


		Iterator<EdgeMST> eIter = edgesList.iterator();

		EdgeMST e = eIter.next();
		int level = (e != null) ? e.getLevel() : 1;

		ArrayList<Integer> vaux = new ArrayList<Integer>(maxid + 1);

		int prev = 0;
		int now = 0;

		while (true) {
			if (e != null && e.getLevel() == level) {
				int u = e.getSource();
				int v = e.getDest();
				if (!vaux.contains(u)) {
					vaux.add(u);
				}
				if (!vaux.contains(v)) {
					vaux.add(v);
				}
				int s = map[u];
				int d = map[v];

				matrix.set(s, d, matrix.get(s, d) - 1);
				matrix.set(d, s, matrix.get(d, s) - 1);
				matrix.set(s, s, matrix.get(s, s) + 1);
				matrix.set(d, d, matrix.get(d, d) + 1);

				if (!ds.sameSet(u, v)) {
					ds.unionSet(u, v);
				}

				now++;

				try {
					e = eIter.next();
				} catch (NoSuchElementException ex) {
					e = null;
				}

			} else if (prev != now ) {
				mapid = 0;
				for (int i = 0; i <= maxid; i++) {
					int setid = ds.findSet(i);
					if (mapaux[setid] == -1) {
						mapaux[setid] = mapid;
						mapaux[i] = mapid;
						mapid++;
					} else {
						mapaux[i] = mapaux[setid];
					}
				}

				ArrayList[] calcDet = new ArrayList[mapid];
				for (int i = 0; i < calcDet.length; i++) {
					calcDet[i] = new ArrayList<Integer>();
				}

				for (int i = 0; i < vaux.size(); i++) {
					if (!calcDet[mapaux[vaux.get(i)]].contains(map[vaux.get(i)])) {
						calcDet[mapaux[vaux.get(i)]].add(map[vaux.get(i)]);
					}
				}


				double[] calcNMstsDet = new double[mapid];
				for (int i = 0; i < calcDet.length; i++) {

					if (!calcDet[i].isEmpty()) {
						
						// Check Kirchhoff's theorem!
						int[] vgraph = new int[calcDet[i].size() - 1];
						ArrayList<Integer> graph = (ArrayList<Integer>) calcDet[i];
						Iterator<Integer> gIter = graph.iterator();
						int index = 0;
						gIter.next();
						while (gIter.hasNext()) {
							vgraph[index++] = gIter.next();
						}
						
						UpperTriangDenseMatrix u = DenseLU.factorize(Matrices.getSubMatrix(matrix, vgraph, vgraph).copy()).getU();
						double det = 0.0;
						for (int k = 0; k < u.numRows(); k++)
							det += Math.log10(Math.abs(u.get(k, k)));
						
						calcNMstsDet[i] = det;
						nmsts = nmsts + det;
					}
				}

				calcEdgesNMSTs(edgesList, prev, now, map, mapaux, calcDet, matrix, calcNMstsDet);
				prev = now;
				if (e == null) {
					break;
				}

				matrix = new DenseMatrix(mapid, mapid);
				matrix.zero();
				map = mapaux;
				mapaux = new int[maxid + 1];
				for (int i = 0; i <= maxid; i++) {
					mapaux[i] = -1;
				}
				level++;
				vaux = new ArrayList<Integer>(mapid);
			} else { 
				level++;
			}
		}

		return nmsts;
	}
}
