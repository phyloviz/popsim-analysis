package net.phyloviz.nlvgraph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import net.phyloviz.mlst.AbstractArchive;
import net.phyloviz.mlst.Dataset;
import net.phyloviz.mlst.Profile;
import net.phyloviz.mlst.Utils;

public class GraphNLV {

	private Vector<TreeMap<Integer, TreeSet<Integer>>> vNLVGraph;
	private Vector<Set<Integer>> vNLVSTs;

	private AbstractArchive archive;

	public GraphNLV(AbstractArchive archive) {
		this.archive = archive;
		this.vNLVGraph = new Vector<TreeMap<Integer, TreeSet<Integer>>>();
		this.vNLVSTs = new Vector<Set<Integer>>();
		for (int i = 0; i < Profile.PROFILE_SIZE; i++) {
			this.vNLVGraph.add(i, new TreeMap<Integer, TreeSet<Integer>>());
			this.vNLVSTs.add(i, new TreeSet<Integer>());
		}
	}

	public void build(int maxLevel) {
		List<Integer> alUniqueSTs = new ArrayList<Integer>(
				this.archive.getAllSTids());
		for (int i = 0; i < (alUniqueSTs.size() - 1); i++) {
			for (int j = i + 1; j < alUniqueSTs.size(); j++) {
				int st1 = alUniqueSTs.get(i);
				int st2 = alUniqueSTs.get(j);
				int diff = this.archive.getProfile(st1).getNdiff(
						this.archive.getProfile(st2));

				if (diff > maxLevel || diff > Profile.PROFILE_SIZE || diff < 1)
					continue;
				this.putInTreeSet(this.vNLVGraph.elementAt(diff - 1), st1, st2);
				this.vNLVSTs.elementAt(diff - 1).add(st1);
				this.vNLVSTs.elementAt(diff - 1).add(st2);
			}
		}
	}

	public Collection<Integer> getNLVAdjacency(int level, int index) {
		return vNLVGraph.elementAt(level - 1).get(index);
	}
	
	private void putInTreeSet(TreeMap<Integer, TreeSet<Integer>> tm, int st1,
			int st2) {
		TreeSet<Integer> tsTmp;
		// Unidirectional
		// int min = st1, max = st2;
		// if (st2 < st1) {
		// min = st2;
		// max = st1;
		// }
		// if (tm.containsKey(min) && tm.get(min).contains(max))
		// return;
		// if (!tm.containsKey(min))
		// tsTmp = new TreeSet<Integer>();
		// else
		// tsTmp = tm.get(min);
		// tsTmp.add(max);
		// tm.put(min, tsTmp);

		// Bidirectional
		if (!tm.containsKey(st1))
			tsTmp = new TreeSet<Integer>();
		else
			tsTmp = tm.get(st1);
		tsTmp.add(st2);
		tm.put(st1, tsTmp);

		if (!tm.containsKey(st2))
			tsTmp = new TreeSet<Integer>();
		else
			tsTmp = tm.get(st2);
		tsTmp.add(st1);
		tm.put(st2, tsTmp);
	}

	/**
	 * Computes the recombination links on an SLV graph. It goes through the DLV
	 * graph and gets the SLV intersection between two DLV-vertices.
	 * 
	 * @param archive
	 * @return
	 */
	public HashSet<String> computeHomoplasies() {
		TreeMap<Integer, TreeSet<Integer>> tmSLVs = vNLVGraph.elementAt(0);
		TreeMap<Integer, TreeSet<Integer>> tmDLVs = vNLVGraph.elementAt(1);
		ArrayList<Integer> alDLVs = new ArrayList<Integer>(vNLVGraph.elementAt(
				1).keySet());
		HashSet<String> hsHomoSquares = new HashSet<String>();

		for (int i = 0; i < (alDLVs.size() - 1); i++) {
			// Founder ST
			int stf = alDLVs.get(i);
			for (int j = i + 1; j < alDLVs.size(); j++) {
				// Recombinant ST
				int std = alDLVs.get(j);
				// If (f-dlv-d)
				if (!tmDLVs.get(stf).contains(std))
					continue;
				// If (f-slv-? && d-slv-?)
				if (tmSLVs.containsKey(stf) && tmSLVs.containsKey(std)) {
					ArrayList<Integer> alTmp = new ArrayList<Integer>();
					for (int st : tmSLVs.get(stf))
						if (tmSLVs.get(std).contains(st))
							alTmp.add(st);
					if (alTmp.size() == 2) {
						alTmp.add(stf);
						alTmp.add(std);
						Collections.sort(alTmp);
						String sTmp = alTmp.get(0) + "\t" + alTmp.get(1) + "\t"
								+ alTmp.get(2) + "\t" + alTmp.get(3);
						hsHomoSquares.add(sTmp);
					} else if (alTmp.size() > 2) {
						// Fazer as combinacoes
						for (int k = 0; k < alTmp.size() - 1; k++)
							for (int l = k + 1; l < alTmp.size(); l++) {
								ArrayList<Integer> aux = new ArrayList<Integer>();
								aux.add(stf);
								aux.add(std);
								aux.add(alTmp.get(k));
								aux.add(alTmp.get(l));
								Collections.sort(aux);
								String sTmp = aux.get(0) + "\t" + aux.get(1)
										+ "\t" + aux.get(2) + "\t" + aux.get(3);
								hsHomoSquares.add(sTmp);
							}
					} // else not a square(s)
				}
			}
		}
		return hsHomoSquares;
	}

	private String getStats() {
		String s = "Unique STs: " + archive.getAllSTids().size();
		for (int i = 0; i < vNLVGraph.size(); i++) {
			if (vNLVGraph.get(i).size() == 0)
				break;
			// SLV graph is bidirectional ==> #links / 2
			int links = 0;
			for (int st : vNLVGraph.elementAt(i).keySet()) {
				links += vNLVGraph.elementAt(i).get(st).size();
			}
			s += "\n" + (i + 1) + "LV\tSTs: " + vNLVSTs.get(i).size()
					+ "\tlinks: " + (links / 2);
		}
		return s;
	}

	public void getHomoplasiesSeqDiff(Set<String> hsHomoplasies,
			ArrayList<TreeMap<Integer, String>> alSeqs, String sFile) {
		System.out.println("Computing Homoplasies sequence differences...");
		ArrayList<String> alSeqDiffs = this.computeHomoplasiesSeqDiffs(archive,
				alSeqs, hsHomoplasies);

		StringBuffer sb = new StringBuffer("min\tmax\n");
		for (String s : alSeqDiffs) {
			sb.append(s).append("\n");
		}
		Utils.writeFile(sFile + ".slv-graph-homoplasies-seq-diffs",
				sb.toString());
		String baseName = sFile.substring(sFile.lastIndexOf("/") + 1);
		sb = new StringBuffer("#!/bin/bash\nR --no-save <<EOF\n");
		sb.append("data <- read.table(\"").append(baseName)
				.append(".slv-graph-homoplasies-seq-diffs\"")
				.append(", header=TRUE)\npdf(\"").append(baseName)
				.append(".slv-graph-homoplasies-seq-diffs.pdf\", ")
				.append("paper=\"a4r\", width=0, height=0)\nd<-")
				.append("data[order(data[,2], data[,1], decreasing=TRUE),]\n")
				.append("plot(d[,1], col=\"blue\", pch=4, xlab=\"\", ylab=")
				.append("\"\", ylim=c(0,max(d)))\npar(new=TRUE)\nplot(d[,2], ")
				.append("col=\"red\", pch=3, ylim=c(0,max(d)), xlab=\"Number ")
				.append("of homoplasy squares\", ylab=\"Nucleotide difference")
				.append("\", main=paste(\"Min&Max nucleotide diff in ")
				.append("homoplasy squares:\", \"").append(baseName)
				.append("\"))\npar(new=TRUE)\nabline(h=c(0:max(d)), col=")
				.append("\"gray\")\ndev.off()\nEOF");

		Utils.writeFile(sFile + ".slv-graph-homoplasies-seq-diffs.sh",
				sb.toString());
	}

	private ArrayList<String> computeHomoplasiesSeqDiffs(
			AbstractArchive archive,
			ArrayList<TreeMap<Integer, String>> alSeqs,
			Set<String> hsHomoplasies) {
		ArrayList<String> alDiffs = new ArrayList<String>();

		for (String s : hsHomoplasies) {
			String[] saSTs = s.split("\t");
			int st1 = Integer.parseInt(saSTs[0]);
			Profile pf1 = archive.getProfile(st1);
			int st2 = st1;
			Profile pf2 = pf1;
			for (int i = 1; i < saSTs.length; i++) {
				st2 = Integer.parseInt(saSTs[i]);
				pf2 = archive.getProfile(st2);
				if (pf1.getNdiff(pf2) == 2)
					break;
			}
			// st1 & st2 -> share 2 allelic differences
			int posA = -1, posB = -1;
			int a1 = 0, a2 = 0, b1 = 0, b2 = 0;
			for (int i = 0; i < Profile.PROFILE_SIZE; i++)
				if (pf1.getAlleleIDatPos(i) != pf2.getAlleleIDatPos(i))
					if (posA < 0) {
						posA = i;
						a1 = pf1.getAlleleIDatPos(i);
						a2 = pf2.getAlleleIDatPos(i);
					} else {
						posB = i;
						b1 = pf1.getAlleleIDatPos(i);
						b2 = pf2.getAlleleIDatPos(i);
						break;
					}
			// aA & aB -> 2 alleles that differ

			int diffA = StringUtils.getLevenshteinDistance(alSeqs.get(posA)
					.get(a1), alSeqs.get(posA).get(a2));
			int diffB = StringUtils.getLevenshteinDistance(alSeqs.get(posB)
					.get(b1), alSeqs.get(posB).get(b2));
			if (diffA < diffB)
				alDiffs.add(diffA + "\t" + diffB);
			else
				alDiffs.add(diffB + "\t" + diffA);
		}
		return alDiffs;
	}

	/**
	 * FIXME: worst case scenario: O(n^2) -> not very efficient
	 */
	public ArrayList<TreeSet<Integer>> computeSLVGraphCCs() {
		System.out.println("SLV graph: building CCs (with ?tons)...");
		ArrayList<TreeSet<Integer>> alSLVCCs = new ArrayList<TreeSet<Integer>>();
		for (int st : vNLVGraph.elementAt(0).keySet()) {
			TreeSet<Integer> tsSLVlist = new TreeSet<Integer>();
			tsSLVlist.add(st);
			if (vNLVGraph.elementAt(0).get(st) != null)
				tsSLVlist.addAll(vNLVGraph.elementAt(0).get(st));

			ArrayList<Integer> alPos = new ArrayList<Integer>();
			for (int i = 0; i < alSLVCCs.size(); i++) {
				for (int iST : tsSLVlist)
					if (alSLVCCs.get(i).contains(iST)) {
						alPos.add(i);
						break;
					}
			}
			if (!alPos.isEmpty()) {
				Collections.sort(alPos, Collections.reverseOrder());
				for (int iPos : alPos) {
					tsSLVlist.addAll(alSLVCCs.get(iPos));
					alSLVCCs.remove(iPos);
				}
			}
			alSLVCCs.add(tsSLVlist);
		}

		// Find biggest CC and filter by size
		int bigCCpos = 0, bigCCsz = 0;
		ArrayList<TreeSet<Integer>> alFiltSLVCCs = new ArrayList<TreeSet<Integer>>();
		for (int cc = 0; cc < alSLVCCs.size(); cc++) {
			int size = alSLVCCs.get(cc).size();
			if (size > 0) { // TODO: put 2 to remove doubletons?
				alFiltSLVCCs.add(alSLVCCs.get(cc));
				if (size > bigCCsz) {
					bigCCsz = size;
					bigCCpos = alFiltSLVCCs.size() - 1;
				}
			}
		}

		// Moves big CC to the beginning of the list
		TreeSet<Integer> bigCC = alFiltSLVCCs.get(bigCCpos);
		alFiltSLVCCs.remove(bigCCpos);
		alFiltSLVCCs.add(0, bigCC);
		return alFiltSLVCCs;
	}

	public String compactness(ArrayList<TreeSet<Integer>> alFiltSLVCCs) {
		TreeMap<Integer, Float> tmCompact = new TreeMap<Integer, Float>();
		float avgTotal = 0, avgCC = 0;

		// for each CC
		for (TreeSet<Integer> cc : alFiltSLVCCs) {
			int ccSize = cc.size();
			// Compute the compactness index of each ST
			// System.out.println(cc);
			for (int st : cc) {
				// System.out.println(st + " - " +
				// vNLVGraph.elementAt(0).get(st));
				int degree = vNLVGraph.elementAt(0).get(st).size();
				float c = (float) degree / (ccSize - 1);
				tmCompact.put(st, c);
			}
			// First CC is the Biggest CC
			if (avgCC == 0) {
				for (int st : tmCompact.keySet()) {
					avgCC += tmCompact.get(st);
				}
				avgCC /= tmCompact.size();
			}
		}
		for (int st : tmCompact.keySet()) {
			avgTotal += tmCompact.get(st);
		}
		avgTotal /= tmCompact.size();

		String s = "Average compactness\n\tTotal: " + avgTotal + "\n\tBigCC: "
				+ avgCC;
		return s;
	}

	public String clustering(ArrayList<TreeSet<Integer>> alFiltSLVCCs) {
		TreeMap<Integer, Float> tmCluster = new TreeMap<Integer, Float>();
		float avgTotal = 0, avgCC = 0;

		// for each CC
		for (TreeSet<Integer> cc : alFiltSLVCCs) {
			// Compute the clustering index of each ST
			for (int origST : cc) {
				ArrayList<Integer> neighOrigST = new ArrayList<Integer>(
						vNLVGraph.elementAt(0).get(origST));
				int ei = 0, ki = neighOrigST.size();
				// Upper triangular of origST neighbors
				for (int j = 0; j < (ki - 1); j++) {
					for (int k = j + 1; k < ki; k++) {
						int stj = neighOrigST.get(j), stk = neighOrigST.get(k);
						if (vNLVGraph.elementAt(0).containsKey(stj)
								&& vNLVGraph.elementAt(0).get(stj)
										.contains(stk))
							ei++;
					}
				}
				float c = (ei == 0) ? 0 : (float) (2 * ei) / (ki * (ki - 1));
				tmCluster.put(origST, c);
			}
			// First CC is the Biggest CC
			if (avgCC == 0) {
				for (int st : tmCluster.keySet()) {
					avgCC += tmCluster.get(st);
				}
				avgCC /= tmCluster.size();
			}
		}
		for (int st : tmCluster.keySet()) {
			avgTotal += tmCluster.get(st);
		}
		avgTotal /= tmCluster.size();

		String s = "Average clustering\n\tTotal: " + avgTotal + "\n\tBigCC: " + avgCC;
		return s;
	}

	public static void main(String argv[]) throws IOException {
		AbstractArchive archive;
		if (argv != null && argv.length >= 1) {
			archive = Dataset.loadProfileFile(argv[0]);
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			archive = Dataset.loadProfile(br);
		}

		GraphNLV graph = new GraphNLV(archive);
		// Build until level n
		graph.build(2);
		System.out.println(graph.getStats());

		Set<String> homoplasies = graph.computeHomoplasies();
		System.out.println("Homoplasy squares: " + homoplasies.size());

		if (argv.length > 1 && argv[1].equals("diff")) {
			ArrayList<TreeMap<Integer, String>> alSeqs = Utils
					.loadAllelicSequences(argv[0]);

			graph.getHomoplasiesSeqDiff(homoplasies, alSeqs, argv[0]);
		} else if (argv.length > 1
				&& (argv[1].equals("compactness") || argv[1]
						.equals("clustering"))) {
			ArrayList<TreeSet<Integer>> slvGraphCCs = graph
					.computeSLVGraphCCs();
			System.out.println(graph.compactness(slvGraphCCs));
			System.out.println(graph.clustering(slvGraphCCs));
		}

	}
}
